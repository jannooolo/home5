import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   public static void main (String[] param) {
      String s = "(a(hh)jj,c3)5";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }

   public static void error(String s){
      //If lausete kontroll, et välistada:
         //Veatade /t jaoks
      if (s.contains("\t")) {
         throw new RuntimeException("Tabulaator " + s);
         //veateade, et kasutusel on topelt komad
      } else if (s.contains(",,")) {
         throw new RuntimeException("Mitmekordsed komad avaldises " + s);
         //Kui kasutusel on tühik
      } else if (s.contains(" ")) {
         throw new RuntimeException("sisendiks on tühik " + s);
         //Kui alampuu on tühi
      } else if (s.contains("()")) {
         throw new RuntimeException("Sisendik on tühi alampuu " + s);
         //kui sisaldab kahte juurt
      } else if (s.contains(",") && !(s.contains("(") && s.contains(")"))) {
         throw new RuntimeException("Kaks juurt " + s);
         //kui sisaldab sulge
      } else if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("Sisaldab sulgusi " + s);
      }
   }

   public static Node parsePostfix (String s) {

      error(s);
      String[] tokens = s.split("");
      Stack<Node> stack = new Stack();
      Node node = new Node(null, null, null);
      boolean replacingRoot = false;
      for (int i = 0; i < tokens.length; i++) {
         String token = tokens[i].trim();
         if (token.equals("(")) {
            if (replacingRoot) {
               throw new RuntimeException("Proovite juurt asendada");
            }
            stack.push(node);
            node.firstChild = new Node(null, null, null);
            node = node.firstChild;
            if (tokens[i+1].trim().equals(",")) {
               throw new RuntimeException("Koma peale sõlme ");
            }
         } else if (token.equals(")")) {
            node = stack.pop();
            if (stack.size() == 0) {
               replacingRoot = true;
            }
         } else if (token.equals(",")) {
            if (replacingRoot) {
               throw new RuntimeException("Proovite juurt asendada");
            }
            node.nextSibling = new Node(null, null, null);
            node = node.nextSibling;
         } else {
            if (node.name == null) {
               node.name = token;
            } else {
               node.name += token;
            }
         }
      }
      return node;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(name);
      if (firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (nextSibling != null) {
         sb.append(",");
         sb.append(nextSibling.leftParentheticRepresentation());
      }
      return sb.toString();
   }


}
//kasutatut materjal
//https://www.youtube.com/watch?v=H5JubkIy_p8
//http://docs.oracle.com/javase/8/docs/api/org/w3c/dom/Node.html
//https://git.wut.ee/i231/home5/src/master